/**
 * @Date:   2018-02-05T20:43:49+01:00
 * @Last modified time: 2018-02-05T20:54:53+01:00
 */



'use strict'

/**
 * @namespace datacollector
 * @memberof MTLG
 *
 **/

window.MTLG = (function (m) {
  var datacollector = (function () {
    var _baseUrl = 'http://localhost:10010/'
    /*
     * Initializes the datacollector module in MTLG
     * @memberof MTLG.datacollector#
     *
     * @param {?object} options Basic game options to initalize the module.
     */
    function init (options) {
      if (options) {
        _baseUrl = options.LCDMbaseUrl || _baseUrl
      }
      console.log('datacollector module loaded.')
    }

    /*
     * Returns information about this module
     * @memberof MTLG.datacollector#
     *
     * @return {!object} Object with name and note attributes
     */
    function info () {
      return {
        name: 'datacollector modul',
        note: 'MTLG-Modul that allows to track user interaction and events.'
      }
    }

    /**
     * @callback writeSuccessCallback
     * @memberof MTLG.datacollector#
     */

    /**
     * @callback writeErrorCallback
     * @param {!number} responseCode The status code of the response.
     * @param {!object} responseBody The body of the response.
     * @memberof MTLG.datacollector#
     */

    /**
     * Registers a new user on the platform
     * @param {!string} username The username of the user.
     * @param {!string} password The password of the user.
     * @param {?string} email The email of the user, may be null.
     * @param {!writeSuccessCallback} success The callback that handles a successful response.
     * @param {!writeErrorCallback} error The callback that handles an error response.
     * @memberof MTLG.datacollector#
     */
    function registerUser (username, password, email, success, error) {
      var body = { name: username, pass: password }
      if (email) { body.email = email }
      var request = new window.XMLHttpRequest()
      request.onreadystatechange = function () {
        if (request.readyState !== window.XMLHttpRequest.DONE) { return }
        if (request.status === 201) {
          success()
        } else {
          error(request.status, JSON.parse(request.responseText))
        }
      }
      request.open('POST', _baseUrl + 'lcdm/user')
      request.setRequestHeader('Accept', 'application/json')
      request.setRequestHeader('Content-Type', 'application/json')
      request.send(JSON.stringify(body))
    }

    /**
     * Stores events on the platform
     * @param {!object[]} events An array of objects describing the events to store.
     * Check the LCDM documentation for required and optional fields.
     * @param {!object} token The authentication token for the user.
     * @param {!writeSuccessCallback} success The callback that handles a successful response.
     * @param {!writeErrorCallback} error The callback that handles an error response.
     * @memberof MTLG.datacollector#
     */
    function storeEvents (events, token, success, error) {
      var request = new window.XMLHttpRequest()
      request.onreadystatechange = function () {
        if (request.readyState !== window.XMLHttpRequest.DONE) { return }
        if (request.status === 201) {
          success()
        } else {
          error(request.status, JSON.parse(request.responseText))
        }
      }
      request.open('POST', _baseUrl + 'lcdm/events')
      request.setRequestHeader('Accept', 'application/json')
      request.setRequestHeader('Content-Type', 'application/json')
      request.setRequestHeader('Authorization', 'Bearer ' + token.token)
      request.send(JSON.stringify(events))
    }

    /**
     * @callback authenticateUserCallback
     * @param {!Error} err Null if the credentials are valid,
     * an Error wrapping the body of the response otherwise.
     * @param {!object} token The token you can use to access the API authenticated as the given user.
     * Note that this object should be considered opaque and its structure must not be replied upon.
     * @memberof MTLG.datacollector#
     */

    /**
     * Retrieves an authentication token for the user with the given credentials.
     * The token can be used to make requests to the API.
     * @param {!string} username The username of the user.
     * @param {!string} password The password of the user.
     * @param {!authenticateUserCallback} cb The callback.
     * @memberof MTLG.datacollector#
     */
    function authenticateUser (username, password, cb) {
      var body = { name: username, pass: password }
      var request = new window.XMLHttpRequest()
      request.onreadystatechange = function () {
        if (request.readyState !== window.XMLHttpRequest.DONE) { return }
        if (request.status === 200) {
          const response = JSON.parse(request.responseText)
          cb(null, { token: response.token })
        } else {
          cb(new Error(request.responseText), null)
        }
      }
      request.open('POST', _baseUrl + 'authenticate')
      request.setRequestHeader('Accept', 'application/json')
      request.setRequestHeader('Content-Type', 'application/json')
      request.send(JSON.stringify(body))
    }

    if (m.addModule) { m.addModule(init, info) }

    return {
      info: info,
      init: init,
      registerUser: registerUser,
      storeEvents: storeEvents,
      authenticateUser: authenticateUser
    }
  })()

  m.datacollector = datacollector

  return m
})(window.MTLG || {})
